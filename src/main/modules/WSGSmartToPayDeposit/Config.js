define ( function () {

	'use strict';

	var Config = [ '$log', 

		function ( $log ) {

		return {
			urls : {
				'smarttopayURL' : 'https://apitest.smart2pay.com'
			}
		}

	} ];
	return Config;
});
