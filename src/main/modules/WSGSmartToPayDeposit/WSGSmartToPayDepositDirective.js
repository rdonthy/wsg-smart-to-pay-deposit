define(function () {
  var path=localStorage.path;
  var WSGSmartToPayDepositDirective = function () {

    return {
      restrict: 'A',
      templateUrl: path+'/WSGSmartToPayDeposit.html',
      controller: 'WSGSmartToPayDepositControllerV1'
    };
  };

  return WSGSmartToPayDepositDirective;
});
