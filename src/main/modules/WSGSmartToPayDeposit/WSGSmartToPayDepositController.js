define(['jquery'], function (jquery) {
  var WSGSmartToPayDepositController = [
    '$scope',
    '$filter',
    'WSGSmartToPayDepositConfigFactory',
    '$timeout',
    '$state',
    function ($scope,
              $filter,
              WSGSmartToPayDepositConfigFactory,
              $timeout,$state) {

      var depositDetails = undefined;
      var retrieveDepositDetails = function () {
        if (!sessionStorage.depositDetails) {
          $state.go('deposit');
          return;
        }

        depositDetails = JSON.parse(sessionStorage.depositDetails);
        
      };
      retrieveDepositDetails();

      $scope.merchant_id = undefined;
      $scope.merchant_trans_id = undefined;
      $scope.txn_amount = undefined;
      $scope.txn_currency = undefined;
      $scope.return_url = undefined;
      $scope.skip_hpp = undefined;
      $scope.payment_method = undefined;
      $scope.hash_id = undefined;
      $scope.customer_name = undefined;
      $scope.customer_email = undefined;
      $scope.country = undefined;
      $scope.language = undefined;
      $scope.siteId = undefined;
      
      var processTxnWithProcessor = function () {
    	  
    	  document.giropayDepositForm.action = WSGSmartToPayDepositConfigFactory.urls.smarttopayURL;
    	  
        $scope.merchant_id = depositDetails.custom2;
        $scope.merchant_trans_id = depositDetails.txnId;
        $scope.txn_amount = depositDetails.custom3;
        $scope.txn_currency = depositDetails.currency;
        $scope.payment_method = depositDetails.custom4;
        $scope.skip_hpp = depositDetails.custom5;
        $scope.return_url = depositDetails.custom6;
        $scope.hash_id = depositDetails.custom7;
        $scope.customer_name = depositDetails.custom8;
        $scope.customer_email = depositDetails.custom1;
        $scope.country = depositDetails.custom9;
        $scope.language = depositDetails.custom10;
        $scope.siteId = depositDetails.custom11;
        
        $timeout(function () { 
          jquery('#deposit-form').submit();
        });
      };
      processTxnWithProcessor();
    }
  ];

  return WSGSmartToPayDepositController;
});
