define(['angular',
        'WSGSmartToPayDeposit/Config',
        'WSGSmartToPayDeposit/WSGSmartToPayDepositController',
        'WSGSmartToPayDeposit/WSGSmartToPayDepositDirective',
      ], function (angular,
                   Config,
                   Controller,
                   Directive) {

        var WSGSmartToPayDeposit = angular.module('wsg.WSGSmartToPayDeposit.v1.0.0', []);

        //services
        //Add service declarations here

        //controllers
        //Add controller delcarations here
        WSGSmartToPayDeposit.controller('WSGSmartToPayDepositControllerV1', Controller);

        //directives
        //Add directive declarations here
        //wantedly added small wsg as the directive name should start with small letter
        WSGSmartToPayDeposit.directive('wsgSmartToPayDepositDirectiveV1', Directive);

        //filters
        //Add filter declarations here

        //factories
        //Add factory declarations here
        WSGSmartToPayDeposit.factory('WSGSmartToPayDepositConfigFactory', Config);

        //animations
        //Add animation declarations here

        return WSGSmartToPayDeposit;
      });
