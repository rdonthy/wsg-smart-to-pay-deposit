(function (require) {
	"use strict";
	require.config({
		paths : {
			'requireLib' : '../../../bower_components/requirejs/require',
			'angular' : '../../../bower_components/angular/angular',
			'angular-route' : '../../../bower_components/angular-route/angular-route.min',
			'angular-cookies' : '../../../bower_components/angular-cookies/angular-cookies.min',
			'underscore' : '../../../bower_components/underscore-amd/underscore',
			'socket.io' : '../../../bower_components/socket.io-client/dist/socket.io',
			'angular-nggrid' : '../../../bower_components/ng-grid/build/ng-grid.min',
			'jquery' : '../../../bower_components/jquery/dist/jquery.min'
		},
		shim : {
			'angular' : { deps : [ 'jquery' ], exports : 'angular' },
			'angular-route' : { deps : [ 'angular' ]},
			'angular-cookies' : { deps : [ 'angular' ]},
			'angular-animate' : { deps : [ 'angular' ]},
			'socket.io' : { deps : [], exports : 'io' },
			'angular-nggrid' : { deps : [ 'angular' ] }
		}
	});

}(require));
